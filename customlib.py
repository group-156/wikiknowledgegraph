import re
class Customlib:
    def __init__(self):
        print("created customlib onject")
    def normalize(self,content):
        content = content.replace('\xa0',' ')
        content = content.replace('\n',' ')
        content = content.replace('\r',' ')
        content = content.replace('\t',' ')
        content = content.replace('•','-')
        content = content.replace('•','-')
        content = content.replace('·','.')
        content = content.replace('^',' ')
        content = content.replace('&nbsp',' ')
        noise_regex = r'[\\^~#%&*{}/:<>?|\"-]'
        noise_pattern = re.compile(noise_regex)
        content = noise_pattern.sub(" ",content)
        return content
    
    def normalize_sentence(self,line):
        if len(line)<5:
            return line
        sent = ""
        words = []
        for token in line.split(' '):
            if token != " " and token!= '':
                words.append(token)
        for word in words:
            sent = sent + " " + word
        return sent
    def collect_sentence(self,lines):
        sentences = []
        for line in lines:
            line = self.normalize_sentence(line)
            if len(line)<25:
                continue
            else:
                sentences.append(line)
        return sentences
    ###
    # token list ms have the form [[()]] so the list may be 2 or more sentence
    # check_token must have the form [()] so it could be more than 1 token 
    # but in this case, only consider it always has 1 token
    ###
    def check_entity_in_list_of_pos(self,tokenlist,check_token):
        for sentence in tokenlist:
            for token in sentence:
                for ctoken in check_token:
                    if token[0] == ctoken[0]: 
                        return True
        return False
    ###
    # Token list should be a list of words which already had pos tag in each token.
    ###
    def get_index_of_entity_in_token_list(self,tokenlist,check_token):
        for sentence in tokenlist:
            position = []
            limit = len(sentence)
            # print(check_token[0][0])
            for i in range(0,limit):
                if sentence[i][0] == check_token[0][0]:
                    position.append(i)
                    # print(i)
            return position

    def get_all_entity(self,tokens):
        avai_ner=[]
        for _token in tokens:
            if len(_token) == 0:
                continue
            else:
                ners = []
                cur_ner = ""
                isEntity = 0
                cur_type = ""
                for token in _token:
                    if token[1] != 'O' and isEntity == 0:
                        cur_ner = cur_ner + token[0]
                        cur_type = token[1]
                        isEntity = 1
                        ners.append((cur_ner,cur_type))
                    elif token[1] != 'O' and isEntity == 1:
                        cur_ner = cur_ner +" "+ token[0]
                        ners.pop()
                        ners.append((cur_ner,cur_type))
                    elif token[1] == 'O' and isEntity == 1:
                        ners.pop()
                        ners.append((cur_ner,cur_type))
                        cur_ner = ""
                        isEntity = 0
                        cur_type = ""
                    # print(ners)
                avai_ner.extend(ners)
        # print(avai_ner)
        return avai_ner
    ###
    # These Methods is use to simply get verb between 2 entity in a sentence
    ###
    def find_pos_pairs(self,main_position,sub_position):
        def sortable(e):
            return e['index']
        
        pairs = []
        list_index = []
        for main_pos in main_position:
            list_index.append({'index':main_pos,'type':1})
        for sub_pos in sub_position:
            list_index.append({'index':sub_pos,'type':0})
        list_index.sort(key=sortable)

        num_index = len(list_index)
        for i in range(0,num_index-1):
            if list_index[i]['type'] != list_index[i+1]['type']:
                pairs.append( [ list_index[i]['index'] , list_index[i+1]['index'] ] )
        if len(pairs) == 0:
            print(list_index)
        return pairs
    def is_contain_V(self,token,left_limit,right_limit):
        for i in range(left_limit,right_limit):
            if token[i][1] == 'V':
                return True
        return False
    def get_sub_list_of_tokens(self,tokens,left_limit,right_limit):
        token_list = []
        for i in range(left_limit,right_limit+1):
            token_list.append(tokens[i])
        return self.merger_relation(token_list)
    def get_simple_relation(self,token,ner,main_position,sub_position):
        if len(sub_position)==0:
            return None

        all_relations = []
        all_pairs = self.find_pos_pairs(main_position,sub_position)
        # all_pairs = []
        # all_pairs.extend(left_pairs)
        print(all_pairs)
        verbset = []
        for pair in all_pairs:
            relation = []
            verbs = []
            if self.is_contain_V(token,pair[0],pair[1]) == True:
                relation = self.get_sub_list_of_tokens(token,pair[0],pair[1])
            for i in range (pair[0],pair[1]):
                
                if token[i][1] == 'V':

                    verbs.append(token[i])

            verb = self.merger_verb(verbs)
            # print(verb)
            if len(verb)!=0:
                verbset.append(verb)
            if len(relation)!=0:
                all_relations.append(relation)
        return (all_relations,verbset)
    ##
    # method use to merge relation from set of text
    ##
    def merger_relation(self,relations):
        final = ""
        for token in relations:
            final = final+" "+token[0]
        return final
    def merger_verb(self,relations):
        final = ""
        for token in relations:
            final = final+","+token[0]
        return final