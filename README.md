Instruction for question answer with wikipedia data
I. Requirements
    1. download and install VnCoreNLP tools at https://github.com/vncorenlp/VnCoreNLP
    2. install vncorenlp package in python 3. run command "pip3 install vncorenlp" for Linux and "pip install vncorenlp" for Window OS
    3. download and install spark in the same directory as VnCoreNLP
    4. install mySql server
    5. execute file db_script.sql to generate the database schema.
    6. initiate relation data by execute file init_relation_data.sql
II answer the query
    1. open command window (for Window OS) or terminal ( for Linux system ) and run the command:
        vncorenlp -Xmx2g {full path to VnCoreNLP}\VnCoreNLP\VnCoreNLP-1.1.1.jar -p 9000 -a "wseg,pos,ner,parse"
    2. run command "python answerQuery.py" and type the input questions
