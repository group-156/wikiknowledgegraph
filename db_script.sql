-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema wiki_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema wiki_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wiki_db` DEFAULT CHARACTER SET utf8 ;
USE `wiki_db` ;

-- -----------------------------------------------------
-- Table `wiki_db`.`entities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`entities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `main_entity` MEDIUMTEXT NULL DEFAULT NULL,
  `main_url` MEDIUMTEXT NULL DEFAULT NULL,
  `sub_entity` LONGTEXT NULL DEFAULT NULL,
  `sub_url` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 41242
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `wiki_db`.`entities_3`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`entities_3` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `main_entity` MEDIUMTEXT NULL DEFAULT NULL,
  `main_url` MEDIUMTEXT NULL DEFAULT NULL,
  `sub_entity` LONGTEXT NULL DEFAULT NULL,
  `sub_url` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 11731
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `wiki_db`.`objects`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`objects` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(45) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `wiki_db`.`raw_page`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`raw_page` (
  `pageid` INT(11) NOT NULL AUTO_INCREMENT,
  `html` LONGTEXT NULL DEFAULT NULL,
  `fullurl` MEDIUMTEXT NULL DEFAULT NULL,
  `title` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`pageid`))
ENGINE = InnoDB
AUTO_INCREMENT = 1794
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `wiki_db`.`raw_page_2`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`raw_page_2` (
  `pageid` INT(11) NOT NULL AUTO_INCREMENT,
  `html` LONGTEXT NULL DEFAULT NULL,
  `fullurl` MEDIUMTEXT NULL DEFAULT NULL,
  `title` MEDIUMTEXT NULL DEFAULT NULL,
  `entities_list` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`pageid`))
ENGINE = InnoDB
AUTO_INCREMENT = 2114
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `wiki_db`.`raw_page_3`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`raw_page_3` (
  `pageid` INT(11) NOT NULL AUTO_INCREMENT,
  `html` LONGTEXT NULL DEFAULT NULL,
  `fullurl` MEDIUMTEXT NULL DEFAULT NULL,
  `title` MEDIUMTEXT NULL DEFAULT NULL,
  `entities_list` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`pageid`))
ENGINE = InnoDB
AUTO_INCREMENT = 416
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `wiki_db`.`relations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wiki_db`.`relations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `mainEntity` INT(11) NULL DEFAULT NULL,
  `subEntity` INT(11) NULL DEFAULT NULL,
  `relationText` TINYTEXT NULL DEFAULT NULL,
  `mainEntityType` VARCHAR(45) NULL DEFAULT NULL,
  `subEntityType` VARCHAR(45) NULL DEFAULT NULL,
  `frequency` INT(11) NULL DEFAULT NULL,
  `beginTime` VARCHAR(45) NULL DEFAULT NULL,
  `endTime` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
