import pymysql
import pymysql.cursors
import logging 
from vncorenlp import VnCoreNLP
from time import sleep
def checkExist(url, list):
    for element in list:
        if url == element:
            return True
    return False
connection = pymysql.connect(host='localhost',
                                user='root',
                                password='12345678',
                                db='wiki_db',
                                charset='utf8',
                                cursorclass=pymysql.cursors.DictCursor)

# use to get all entity from a set of tokens
def get_all_entity(tokens):
    avai_ner=[]
    for _token in tokens:
        if len(_token) == 0:
            continue
        else:
            ners = []
            cur_ner = ""
            isEntity = 0
            cur_type = ""
            for token in _token:
                # print(token)
                if token[1] != 'O' and isEntity == 0:
                    # print(type(token[0]))
                    # print(token[0])
                    cur_ner = cur_ner + token[0]
                    cur_type = token[1]
                    isEntity = 1
                elif token[1] != 'O' and isEntity == 1:
                    # print(token[0])
                    cur_ner = cur_ner +" "+ token[0]
                elif token[1] == 'O' and isEntity == 1:
                    ners.append((cur_ner,cur_type))
                    cur_ner = ""
                    isEntity = 0
                    cur_type = ""
                    # print(ners)
                    # all_ners.extend(ners)
                # for entity in ners:
                #     start_id = line.find(entity[0])
                #     if line[start_id-2] == '@' and line[start_id+len(entity[0])+2]=='@' and checkExist(entity,avai_ner)!= True:
                #         print("{} {} {}".format(line[start_id-3] ,entity[0],line[start_id+len(entity[0])+3]))
        avai_ner.extend(ners)
    return avai_ner
try: 
    with connection.cursor() as cursor:
        annotator = VnCoreNLP(address="http://127.0.0.1", port=9000) 
        sql = "select html from raw_page_2 limit 1"
        cursor.execute(sql)
        content = cursor.fetchone()
        # print(content)
        lines = content["html"].split(".")
        avai_ner=[]
        i = 0
        for line in lines:
            # print(line)
            try:
                tokens = annotator.ner(line)
                single_line_ner = get_all_entity(tokens)
                avai_ner.extend(single_line_ner)
            except Exception as Argument:
                print(len(line)) 
                # print("Exception happen at line {}",format(line))
                # print(str(Argument))
        print(avai_ner)
        annotator.close()
finally:
    connection.close()