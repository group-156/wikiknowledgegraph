import pymysql
import pymysql.cursors
import logging
import sys
import os
sys.path.append(".")
from customlib import Customlib
from vncorenlp import VnCoreNLP
from time import sleep
def checkExist(url, list):
    for element in list:
        if url == element:
            return True
    return False
connection = pymysql.connect(host='localhost',
                                user='root',
                                password='12345678',
                                db='wiki_db',
                                charset='utf8',
                                cursorclass=pymysql.cursors.DictCursor)
annotator = VnCoreNLP(address="http://127.0.0.1", port=9000) 

try: 
    with connection.cursor() as cursor:
        sql = "select count(*) from raw_page_2 where title != ''"
        cursor.execute(sql)
        number_of_page = cursor.fetchone()["count(*)"]
        current_page_index = 0
        amount_of_page_each_parse = 1
        # number_of_page = 1
        a = Customlib()
        while current_page_index < number_of_page:
            
            sql = "select * from raw_page_2 where title != '' limit {begin},{amount}".format(begin=current_page_index,amount=amount_of_page_each_parse)
            cursor.execute(sql)
            pages_info = cursor.fetchall()
            for page in pages_info:
                content = page['html']
                title = page['title']
                content = a.normalize(content)
                sentences = a.collect_sentence(content.split('.'))
                print(len(sentences))
                # print(sentences)

                entity_tokens = annotator.ner(title)
                entities_from_content = a.get_all_entity(tokens=entity_tokens)
                entities_from_database = []
                ## load all entity from database after crawling
                get_ent_sql = "select * from entities where main_entity = '{title_}'".format(title_ = title)
                cursor.execute(get_ent_sql)
                allEntities = cursor.fetchall()
                
                for entity in allEntities:
                    main_entity = entity['main_entity']
                    sub_entity = entity['sub_entity']
                    type_of_main_entity = a.get_all_entity(tokens=annotator.ner(main_entity))
                    type_of_sub_entity = a.get_all_entity(tokens=annotator.ner(sub_entity))
                    entities_from_database.append((main_entity,sub_entity,type_of_main_entity,type_of_sub_entity))
                    # print(entity['main_entity'],entity['sub_entity'])
                posibility = []
                break_point = 0
                for sentence in sentences:
                    if break_point !=0 :
                        break
                    if title in sentence:
                        pos = annotator.pos_tag(sentence)
                        ner = annotator.ner(sentence)
                        for entity in entities_from_database:
                            if a.check_entity_in_list_of_pos(pos,[(entity[1].replace(" ","_"),entity[3])])==True:
                                sub_entity = entity[1].replace(" ","_")
                                main_entity = entity[0].replace(" ","_")
                                
                                main_entity_type = ""
                                sub_entity_type = ""
                                if len(entity[2])!=0:
                                    main_entity_type = entity[2][0][1]
                                if len(entity[3])!=0:
                                    sub_entity_type = entity[3][0][1]
                                sub_position = a.get_index_of_entity_in_token_list(pos,[(sub_entity,"")])
                                main_position = a.get_index_of_entity_in_token_list(pos,[(main_entity,"")])
                                if len(main_position) < 1 or len(sub_position) <1:
                                    continue
                                print(main_entity," ",main_position," ",sub_entity,sub_position)
                                relations = a.get_simple_relation(pos[0],ner[0],main_position,sub_position)
                                print(relations)
                                print(main_entity_type)

                                # for relation in relations[0]:
                                #     rela = a.merger_relation(relation)
                                #     print(rela)
                                for i in range(0,len(relations[0])):
                                    sql = "insert into relationsTest(mainEntity,subEntity,relationText,setOfVerb,mainEntityType,subEntityType,frequency) value ('{mainEntity}','{subEntity}','{relationText}','{setOfVerb}','{mainEntityType}','{subEntityType}',1)".format(
                                        mainEntity=main_entity,
                                        subEntity =sub_entity,
                                        relationText = relations[0][i],
                                        setOfVerb = relations[1][i],
                                        mainEntityType = main_entity_type,
                                        subEntityType = sub_entity_type
                                        )
                                    cursor.execute(sql)
                                print("\n")
                                # for relations
                                # break_point = 0
                                # break
            current_page_index = current_page_index + amount_of_page_each_parse
            # print("hello",current_page_index)
            connection.commit()
finally:
    connection.close()
