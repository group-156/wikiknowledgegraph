use wiki_db;
drop table relationsTest;
CREATE TABLE IF NOT EXISTS `wiki_db`.`relationsTest` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `mainEntity` tinytext NULL DEFAULT NULL,
  `subEntity` tinytext NULL DEFAULT NULL,
  `relationText` longtext NULL DEFAULT NULL,
  `setOfVerb` longtext NULL DEFAULT NULL,
  `mainEntityType` VARCHAR(45) NULL DEFAULT NULL,
  `subEntityType` VARCHAR(45) NULL DEFAULT NULL,
  `frequency` INT(11) NULL DEFAULT NULL,
  `beginTime` VARCHAR(45) NULL DEFAULT NULL,
  `endTime` VARCHAR(45) NULL DEFAULT NULL,
  
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;