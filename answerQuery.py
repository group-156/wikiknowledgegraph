
import pymysql
from customlib import Customlib
from vncorenlp import VnCoreNLP
from gensim.models import KeyedVectors
import numpy as np
connection = pymysql.connect(host='localhost',
                                user='root',
                                password='12345678',
                                db='wiki_db',
                                charset='utf8',
                                cursorclass=pymysql.cursors.DictCursor)
annotator = VnCoreNLP(address="http://127.0.0.1", port=9000) 
link_to_w2v_model = "../wiki.vi.model.bin"
w2v_model = KeyedVectors.load_word2vec_format(link_to_w2v_model,binary=True)
lib_object = Customlib()
if __name__ == "__main__":
    while(true):
        query = input()
        # query_vector = lib_object.sent_vectorize(query,w2v_model)
        postag = annotator.pos_tag(sentence)
        subject = ""
        for token in postag[0]:
            if token[1] == 'N' or token[1] == 'NP':
                subject = token[0]
        if subject =="":
            print("cannot answer this quetion")
        else:
            with connection.cursor() as cursor:
                sql = "select * from relationTest where mainEntity like '%{}%' or subEntity like '%{}%'".format(subject,subject)
                cursor.execute(sql)
                cases = cursor.fetchall()
                sentences = []
                distance = []
                # idx = []
                for case in cases:
                    sentences.append(case[relationText])
                for sentence in sentences:
                    # sub_vector = lib_object.sent_vectorize(sentence,w2v_model)
                    distance.append(w2v_model.wmdistance(query.split(" "),sentence.replace("_"," ").split(" ")))
                min_dist = np.argmin(distance)
                print("the answer is:",sentences[min_dist])
            
            print("do you want to keep asking?(y/n)")
            choice = input()
            if choice =="n" or choice == "N":
                break

            
                